package br.com.vivrecoders;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

/**
 *   Existe um bug na configuração do bus(refresh de config),
 *   onde ele não atrela no rabbitmq os exchange in ao out.
 *   Fazendo o bind na mão funciona
 *   Possivelmente fixado na versão 6 do cloud e 1.5.2 do boot
 * 	 FIXME alterar para as versões mais novas e retestar
 * 
 * @author guilherme
 *
 */
@SpringBootApplication
@EnableConfigServer
public class ArtecrocheApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArtecrocheApplication.class, args);
	}
}
