package br.com.vivrecoders.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.vivrecoders.service.SentenceService;

@RestController
public class SentenceController {
	/*
	 * @Autowired DiscoveryClient client;
	 */
	/*
	 * @Autowired LoadBalancerClient ribbonClient;
	 */

	@Autowired
	private SentenceService sentenceService;

	@RequestMapping("/sentence")
	public @ResponseBody String getSentence() {
		return sentenceService.buildSentence();
	}

	/*
	 * @RequestMapping("/sentence") usando ribbon e resttemplate
	 * public @ResponseBody String getSentence() { return
	 * getWord("artecroche-subject-service") + " " +
	 * getWord("artecroche-verb-service") + "."; }
	 */
	/*
	 * Method to use with eureka public String getWord(String service) {
	 * List<ServiceInstance> list = client.getInstances(service); if (list !=
	 * null && list.size() > 0) { URI uri = list.get(0).getUri(); if (uri !=
	 * null) { return (new RestTemplate()).getForObject(uri, String.class); } }
	 * return null; }
	 */

	/* Method to use with ribbon */
/*	public String getWord(String service) {
		ServiceInstance instance = ribbonClient.choose(service);
		return (new RestTemplate()).getForObject(instance.getUri(), String.class);
	}*/

}
