package br.com.vivrecoders;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArtecrocheClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArtecrocheClientApplication.class, args);
	}
}
