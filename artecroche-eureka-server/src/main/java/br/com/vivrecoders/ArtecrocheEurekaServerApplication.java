package br.com.vivrecoders;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class ArtecrocheEurekaServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArtecrocheEurekaServerApplication.class, args);
	}
}
