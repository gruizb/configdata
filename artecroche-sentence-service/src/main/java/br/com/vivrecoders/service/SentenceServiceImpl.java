package br.com.vivrecoders.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SentenceServiceImpl implements SentenceService {

	@Autowired
	private WordService wordService;

	public String buildSentence() {
		return wordService.getSubject() + " " + wordService.getVerb() + ".";
	}

}
