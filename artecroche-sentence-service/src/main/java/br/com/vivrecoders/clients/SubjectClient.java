package br.com.vivrecoders.clients;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.vivrecoders.Word;

@FeignClient("artecroche-subject-service")
public interface SubjectClient {
	
	@RequestMapping(method = RequestMethod.GET, path = "/")
	public Word getWord();
}
