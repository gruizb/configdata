package br.com.vivrecoders.service;

public interface WordService {

	String getSubject();

	String getVerb();

}
