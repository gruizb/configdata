package br.com.vivrecoders;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@ConfigurationProperties(prefix="wordConfig")
public class ConfigClientController {

	String luckyWord;

	@RequestMapping("/lucky-word")
	public String showLuckyWord() {
		return "The lucky word is: " + getLuckyWord();
	}

	public String getLuckyWord() {
		return luckyWord;
	}

	public void setLuckyWord(String luckyWord) {
		this.luckyWord = luckyWord;
	}
	
	
}