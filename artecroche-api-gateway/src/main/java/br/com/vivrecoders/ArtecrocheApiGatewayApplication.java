package br.com.vivrecoders;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.cloud.netflix.zuul.filters.route.ZuulFallbackProvider;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableCircuitBreaker
@EnableDiscoveryClient
@EnableZuulProxy
@EnableHystrixDashboard
public class ArtecrocheApiGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArtecrocheApiGatewayApplication.class, args);
	}

	@Bean
	public ZuulFallbackProvider verbFallBack() {
		return new VerbFallBack();
	}
	
	@Bean
	public ZuulFallbackProvider subjectFallBack() {
		return new SubjectFallback();
	}
	
	
}
