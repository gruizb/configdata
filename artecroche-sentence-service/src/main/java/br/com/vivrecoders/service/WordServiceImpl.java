package br.com.vivrecoders.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

import br.com.vivrecoders.clients.SubjectClient;
import br.com.vivrecoders.clients.VerbClient;

@Service
public class WordServiceImpl implements WordService {

	@Autowired
	private VerbClient verbClient;

	@Autowired
	private SubjectClient subjectClient;

	@HystrixCommand(fallbackMethod = "getFallbackSubject")
	public String getSubject() {
		return subjectClient.getWord().getWord();
	}

	@HystrixCommand(fallbackMethod = "getFallbackVerb")
	public String getVerb() {
		return verbClient.getWord().getWord();
	}

	public String getFallbackSubject() {
		return "default subject";
	}

	public String getFallbackVerb() {
		return "default verb";
	}

}
